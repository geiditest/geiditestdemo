System.register(['@angular/core', 'ionic-angular', './login.service', '../project/project'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, ionic_angular_1, login_service_1, project_1;
    var LoginClass, LoginPage;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ionic_angular_1_1) {
                ionic_angular_1 = ionic_angular_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (project_1_1) {
                project_1 = project_1_1;
            }],
        execute: function() {
            LoginClass = (function () {
                function LoginClass() {
                }
                return LoginClass;
            }());
            exports_1("LoginClass", LoginClass);
            LoginPage = (function () {
                function LoginPage(loginService, navController) {
                    this.loginService = loginService;
                    this.navController = navController;
                    //console.log(loginService.LoginEmailandPassword("test@yahoo.com","test"));
                }
                LoginPage.prototype.login = function (email, password) {
                    console.log(email + "  " + password);
                    this.isError = this.loginService.LoginEmailandPassword(email, password);
                    if (this.isError != false) {
                        this.navController.push(project_1.ProjectPage, {});
                    }
                };
                LoginPage = __decorate([
                    core_1.Component({
                        //selector: 'login',
                        templateUrl: 'build/pages/login/login.html',
                        providers: [login_service_1.LoginService]
                    }), 
                    __metadata('design:paramtypes', [login_service_1.LoginService, ionic_angular_1.NavController])
                ], LoginPage);
                return LoginPage;
            }());
            exports_1("LoginPage", LoginPage);
        }
    }
});
//angular.module('myApp').controller('loginController',LoginController); 
//# sourceMappingURL=login.js.map