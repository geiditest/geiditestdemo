import { Injectable } from '@angular/core';


@Injectable()
export class LoginService {
  errorMessage = "";
  LoginEmailandPassword(email: string, password: string) { 
     
    console.log("service here " + email + "and" + password)
    if (!this.validateEmail(email))
    {
         alert("Invalid Email Address");
         return false;
    }

     if (password.length < 4)
    {
        alert("Invalid Password length")
        return false;
    }

    return true;


  }

  validateEmail(email: string)
  {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}