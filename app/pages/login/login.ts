import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginService } from './login.service';
import {ProjectPage} from '../project/project';


export class LoginClass {
  email: string;
  password: string;
}


@Component({
  //selector: 'login',
  templateUrl: 'build/pages/login/login.html',
  providers: [LoginService]
})


 export class LoginPage {
   //email: string;
   //password: string;
   isError: boolean;
   constructor(private loginService: LoginService, private navController: NavController) {
       
       //console.log(loginService.LoginEmailandPassword("test@yahoo.com","test"));
   }


  login(email: string, password: string) {
      console.log(email + "  " + password);
      this.isError = this.loginService.LoginEmailandPassword(email,password);
      if (this.isError != false){
          this.navController.push(ProjectPage, {});
      }
  }

   
 }

//angular.module('myApp').controller('loginController',LoginController);