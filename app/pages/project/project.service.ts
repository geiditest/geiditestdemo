import { Injectable } from '@angular/core';


@Injectable()
export class ProjectService {

  ProjectLists = [''];

  getProjectLists() {

    this.ProjectLists = JSON.parse(localStorage.getItem("ProjectLists"));

    return this.ProjectLists;
  }

  saveProjectList(project: string) {

    //this.ProjectLists = JSON.parse(localStorage.getItem("ProjectLists"));
    if (this.ProjectLists.indexOf(project) > -1){
      alert("This project name: " + project + " is already existed..");
      return false 
    }
    
    this.ProjectLists.push(project);
    console.log(this.ProjectLists);
    localStorage.setItem("ProjectLists", JSON.stringify(this.ProjectLists));

  }


}