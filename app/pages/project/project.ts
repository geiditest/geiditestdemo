import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ProjectService} from './project.service';

@Component({
  templateUrl: 'build/pages/project/project.html',
  providers: [ProjectService]
})

export class ProjectPage {
  projects = [''];
  constructor(private projectService: ProjectService,private navController: NavController) {

  	this.projects = projectService.getProjectLists();
  	console.log(this.projects);

  }

  saveProjectList(project: string){

  	this.projectService.saveProjectList(project);

  }

}
