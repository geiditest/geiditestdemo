System.register(['@angular/core', '../login/login', '../project/project'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, login_1, project_1;
    var TabsPage;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (login_1_1) {
                login_1 = login_1_1;
            },
            function (project_1_1) {
                project_1 = project_1_1;
            }],
        execute: function() {
            TabsPage = (function () {
                function TabsPage() {
                    // this tells the tabs component which Pages
                    // should be each tab's root Page
                    this.tab1Root = login_1.LoginPage;
                    this.tab2Root = project_1.ProjectPage;
                }
                TabsPage = __decorate([
                    core_1.Component({
                        templateUrl: 'build/pages/tabs/tabs.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], TabsPage);
                return TabsPage;
            }());
            exports_1("TabsPage", TabsPage);
        }
    }
});
//# sourceMappingURL=tabs.js.map