import {Component} from '@angular/core'
import {LoginPage} from '../login/login';
import {ProjectPage} from '../project/project';

@Component({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  private tab1Root: any;
  private tab2Root: any;

  constructor() {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    this.tab1Root = LoginPage;
    this.tab2Root = ProjectPage;
  }
}

